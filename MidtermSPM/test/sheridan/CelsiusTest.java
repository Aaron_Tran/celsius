package sheridan;

/*
 * @Author Aaron Tran
 * 991525227
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int val = Celsius.fromFahrenheit(41);
		assertTrue("Invalid input", val == 5);
	}
	
	@Test
	public void testFromFahrenheitException() {
		int val = Celsius.fromFahrenheit(123);
		assertFalse("Invalid input", val == 11.6667);
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int val = Celsius.fromFahrenheit(36);
		assertTrue("Invalid input", val == 2);
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int val = Celsius.fromFahrenheit(37);
		assertFalse("Invalid input", val == 3);
	}

}
