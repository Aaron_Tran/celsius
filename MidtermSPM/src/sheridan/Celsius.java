package sheridan;

public class Celsius {
	
	public static int fromFahrenheit(int fahrenheit) throws IllegalArgumentException {
		double celsius =(( 5 *(fahrenheit - 32.0)) / 9.0);
		return (int) celsius;
	}

}
